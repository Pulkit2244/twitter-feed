var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var session = require('express-session');

var index = require('./routes/index');
var users = require('./routes/users');
var auth = require('./routes/auth');
var app = express();
var express_graphql = require('express-graphql');
var { buildSchema } = require('graphql');
var MongoClient = require('mongodb').MongoClient;
// var url ="mongodb://localhost:27017";

// mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/twitter-feed', { promiseLibrary: global.Promise })
  .then(() =>  console.log('connection successful'))
  .catch((err) => console.error(err));

//graphQL part starts
var schema = buildSchema(`
    type Query {
        twitter_Graph(userid: String!): Twitter_Graph
        twitter_Graphs(description: String): [Twitter_Graph]
    },
    type Twitter_Graph {
          name: String
          userid: String
          photo:String
          access_token:String
          refresh_token:String
          screen_name: String
          location: String
          description: String
          url: String
          followers_count:Int
          friends_count:Int
          listed_count:Int
          favourites_count:Int
          statuses_count:Int
          lang:String
  }
`);

  // let twittergraphData;
  // function getAccount(jobID, callBack){
  //     MongoClient.connect(url, { promiseLibrary: global.Promise }, function (err, client) {
  //         var db = client.db('twitter-feed');
  //         if (err!=null){
  //             twittergraphData = err;
  //         } else {
  //             db.collection('accountusers').find().toArray(function(err, docs) { 
  //                 // console.log(docs);   
  //                 twittergraphData = docs;
  //                 // console.log(twittergraphData);
  //             }); 
  //         }
  //     }); 

// function getAccount(jobID, callBack){
// MongoClient.connect(url, function(err, db){
//     if(err) throw err;
//     var dbo = db.db("twitter-feed");
//     dbo.collection("accountusers").find().toArray(function(err, result){
//         if(err) throw err;
//         db.close();
//         return callBack(result);
//     })
// })
// }

// var twittergraphData=getAccount(12345, function(result) {
//     console.log(twittergraphData);
// });
var twittergraphData=null;

console.log(twittergraphData)

var getTwitter_Graph = function(args) { 
    var userid = args.userid;
    return twittergraphData.filter(twitter_Graph => {
        return twittergraphData.userid == userid;
    })[0];
}
var getTwitter_Graphs = function(args) {
    if (args.description) {
        var description = args.description;
        return twittergraphData.filter(twitter_Graph => twitter_Graph.description === description);
    } else {
        return twittergraphData;
    }
}
var updateTwitter_Graphs = function({id, topic}) {
    twittergraphData.map(twitter_Graph => {
        if (twitter_Graph.userid === userid) {
            twitter_Graph.description = description;
            return twitter_Graph;
        }
    });
    return twittergraphData.filter(twitter_Graph => twitter_Graph.userid === userid) [0];
}


var root = {
    twitter_Graph: getTwitter_Graph,
    twitter_Graphs: getTwitter_Graphs
    // updateTwitter_Graphs: updateTwitter_Graphs
};

app.use('/graphql', express_graphql({
    schema: schema,
    rootValue: root,
    graphiql: true
}));
// graphQl part ends
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
  secret: 's3cr3t',
  resave: true,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', users);
app.use('/auth', auth);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;
