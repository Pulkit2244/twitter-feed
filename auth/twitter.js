var passport = require('passport')
  , TwitterStrategy = require('passport-twitter').Strategy;
var AccountUser = require('../models/AccountUser');

passport.serializeUser(function (user, fn) {
  fn(null, user);
});

passport.deserializeUser(function (obj, fn) {
  fn(null,obj)
});

var tw =new TwitterStrategy({
    consumerKey: "wK4xrrMv7CmYwi0T9354E4QOY",
    consumerSecret: "iqpIhM4TLSHajLcwM1xZo57s17plw78DwG1riUD3dfAYPnEQU4",
    callbackURL: "http://ec2-13-232-204-168.ap-south-1.compute.amazonaws.com:8011/auth/twitter/callback"
  },
  function(accessToken, refreshToken, profile,done) {
    // console.log(profile);
    AccountUser.findOrCreate({userid:profile._json.id}, {name:profile._json.name,userid:profile._json.id,photo:profile.photos[0].value,access_token:accessToken,refresh_token:refreshToken,screen_name:profile._json.screen_name,location:profile._json.location,description:profile._json.description,url:profile._json.url,followers_count:profile._json.followers_count,friends_count:profile._json.friends_count,listed_count:profile._json.listed_count,created_at:profile._json.created_at,favourites_count:profile._json.favourites_count,statuses_count:profile._json.statuses_count,lang:profile._json.lang}, function(err, user) {
      if (err) {
        console.log(err); 
        return done(err);
      }
      done(null, user);
    });
  }
);

passport.use(tw);

module.exports = passport;

