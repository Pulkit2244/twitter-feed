var mongoose = require('mongoose');
var createOrUpdate = require('mongoose-create-or-update');
var TweetSchema = new mongoose.Schema({
  user_id: String,
  tweet_id: String,
  tweet_user_id:String,
  text: String,
  entities:{type:Object},
  extended_entities:{type:Object},
  user:{type:Object},
  created_at: {type: Date},
  source: String,
  in_reply_to_status_id: String,
  in_reply_to_user_id: String,
  in_reply_to_screen_name: String,
  quoted_status_id: String,
  is_quote_status: Boolean,
  retweet_count: {type: Number, default: 0},
  favorite_count: {type: Number, default: 0},
  possibly_sensitive:Boolean,
  filter_level:String,
  lang: String,
  updated_at: { type: Date, default: Date.now }
},{ strict: false });

TweetSchema.plugin(createOrUpdate);
module.exports = mongoose.model('Tweets', TweetSchema);

//GraphQL part
// var express = require('express');
// var express_graphql = require('express-graphql');
// var { buildSchema } = require('graphql');

// var TweetSchema = buildSchema(`
//     type Query {
//           user_id: String,
//           tweet_id: String,
//           tweet_user_id:String,
//           text: String,
//           entities:{type:Object},
//           extended_entities:{type:Object},
//           user:{type:Object},
//           created_at: {type: Date},
//           source: String,
//           in_reply_to_status_id: String,
//           in_reply_to_user_id: String,
//           in_reply_to_screen_name: String,
//           quoted_status_id: String,
//           is_quote_status: Boolean,
//           retweet_count: {type: Number, default: 0},
//           favorite_count: {type: Number, default: 0},
//           possibly_sensitive:Boolean,
//           filter_level:String,
//           lang: String,
//           updated_at: { type: Date, default: Date.now}
// `);

// TweetSchema.plugin(buildSchema);
// module.exports = express_graphql.model('Tweets', TweetSchema);