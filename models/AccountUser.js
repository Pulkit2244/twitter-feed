var mongoose = require('mongoose');
var findOrCreate = require('mongoose-findorcreate')
var AccountUserSchema = new mongoose.Schema({
  name: String,
  userid: String,
  photo:String,
  access_token:String,
  refresh_token:String,
  screen_name: String,
  location: String,
  description: String,
  url: String,
  followers_count:Number,
  friends_count:Number,
  listed_count:Number,
  created_at:Date,
  favourites_count:Number,
  statuses_count:Number,
  lang:String,
  updated_at: { type: Date, default: Date.now }
});

AccountUserSchema.plugin(findOrCreate);
module.exports = mongoose.model('AccountUser', AccountUserSchema);

// GraphQL part
// var express = require('express');
// var express_graphql = require('express-graphql');
// var { buildSchema } = require('graphql');

// var AccountUserSchema = buildSchema(`
//     type Query {
//           name: String,
//           userid: String,
//           photo:String,
//           access_token:String,
//           refresh_token:String,
//           screen_name: String,
//           location: String,
//           description: String,
//           url: String,
//           followers_count:Number,
//           friends_count:Number,
//           listed_count:Number,
//           created_at:Date,
//           favourites_count:Number,
//           statuses_count:Number,
//           lang:String,
//           updated_at: { type: Date, default: Date.now }
// `);

// AccountUserSchema.plugin(buildSchema);
// module.exports = express_graphql.model('AccountUser', AccountUserSchema);
